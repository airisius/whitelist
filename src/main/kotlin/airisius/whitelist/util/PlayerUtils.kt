package airisius.whitelist.util

import airisius.whitelist.Whitelist
import org.bukkit.entity.Entity
import org.bukkit.entity.Player

fun Entity.isPluginWhitelisted() = Whitelist.check(uniqueId)
